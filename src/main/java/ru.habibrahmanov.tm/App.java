package Main;

import  Manager.ProjectManager;
import Manager.TaskManager;
import java.util.*;

public class App {

    public static void main(String[] args) {

        System.out.println(UUID.randomUUID().toString());
        Scanner scanner = new Scanner(System.in);
        System.out.println("***WELCOME TO TASK MANAGER***");
        System.out.println("Enter HELP for show all commands.");
        ProjectManager projectManager = new ProjectManager();
        TaskManager taskManager = new TaskManager();
        String sc;

        do {
            sc = scanner.nextLine();

            switch (sc.toLowerCase()) {
                case ("pcr"):
                    System.out.println("[PROJECT CREATE]");
                    System.out.println("ENTER NAME:");
                    projectManager.createProject(scanner.nextLine());
                    System.out.println("[OK]");
                    break;

                case ("pl"):
                    System.out.println("[PROJECT LIST]");
                    projectManager.projectList();
                    break;

                case ("pr"):
                    System.out.println("[PROJECT REMOVE]");
                    System.out.println("ENTER ID PROJECT:");
                    projectManager.removeProject(scanner.nextInt());
                    System.out.println("[OK]");
                    break;

                case ("pe"):
                    System.out.println("[PROJECT EDIT]");
                    System.out.println("ENTER ID");

                    projectManager.editProject(scanner.nextInt());

                    break;

                case ("pcl"):
                    System.out.println("[PROJECT CLEAR]");
                    projectManager.clearProjects();
                    break;

                case ("tcr"):
                    System.out.println("[TASK CREATE]");
                    System.out.println("ENTER TASK NAME");
                    taskManager.addTask(projectManager.getCurProject());
                    break;

                case ("tl"):
                    System.out.println("[TASK LIST]");
                    taskManager.taskList(projectManager.getCurProject());
                    break;

                case ("tr"):
                    System.out.println("[TASK REMOVE]");
                    System.out.println("ENTER ID TASK:");
                    int taskId = scanner.nextInt();
                    taskManager.removeTask(projectManager.getCurProject(), taskId);
                    break;

                case ("te"):
                    System.out.println("[TASK EDIT]");
                    System.out.println("ENTER ID TASK:");
                    int taskIdEd = scanner.nextInt();
                    taskManager.editTask(projectManager.getCurProject(), taskIdEd);
                    break;

                case ("sp"):
                    System.out.println("[SWITCH PROJECT]");
                    System.out.println("ENTER PROJECT ID");
                    projectManager.switchProject(scanner.nextInt());
                    break;

                case ("help"):
                    System.out.println("help: Show all commands");
                    System.out.println("project-clear: Remove all project");
                    System.out.println("project-remove: Remove selected project");
                    System.out.println("project-create: Create new projects");
                    System.out.println("project-list: Shaw all project");
                    System.out.println("switch-project: Switch current project");
                    System.out.println("task-create: Create new task");
                    System.out.println("task-list: Show all tasks");
                    System.out.println("task-remove: Remove selected task");
                    System.out.println("task-edit: Edit selected task");
                    break;
            }
        } while (true);
    }
}