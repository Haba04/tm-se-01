package Manager;

import Entity.Project;
import Entity.Task;
import java.util.Scanner;

public class TaskManager {
    Scanner scanner = new Scanner(System.in);
    private int taskId = 0;

    public Task addTask(Project curProject) {
        Task task = new Task(scanner.nextLine(), ++taskId);

        curProject.getTasks().add(task);
        System.out.println("TASK ID: " + taskId + " ADDED TO PROJECT ID: " + curProject.getId());

        return task;
    }

    public void taskList(Project curProject) {

        if (curProject.getTasks().isEmpty()) {
            System.out.println("LIST EMPTY");
        } else {
            for (int i = 0; i < curProject.getTasks().size(); i++) {
                System.out.println(curProject.getTasks().get(i).getId() + " : " + curProject.getTasks().get(i).getName());
            }
        }
    }

    public void removeTask(Project curProject, int taskId) {
        for (int i = 0; i < curProject.getTasks().size(); i++) {
            if (taskId == curProject.getTasks().get(i).getId()) {
                System.out.println("Entity task " + curProject.getTasks().get(i).getName() + " is removed");
                curProject.getTasks().remove(curProject.getTasks().get(i));
            } else {
                System.out.println("TASK ID " + taskId + " DOES'T EXIST");
            }
        }
    }

    public void editTask(Project curProject, int taskId) {
        for (int i = 0; i < curProject.getTasks().size(); i++) {
            if (taskId == curProject.getTasks().get(i).getId()) {
                System.out.println("ENTER NEW NAME");
                curProject.getTasks().get(i).setName(scanner.nextLine());
                System.out.println("Entity Task id " + curProject.getTasks().get(i).getId() + " renamed, new name: " + curProject.getTasks().get(i).getName());
            } else
                System.out.println("TASK ID " + taskId + " DOES'T EXIST");
        }
    }
}
