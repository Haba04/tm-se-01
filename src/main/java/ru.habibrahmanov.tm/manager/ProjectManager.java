package Manager;

import Entity.Project;
import Entity.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ProjectManager {
    private List<Project> projectsList = new ArrayList<>();
    private int projectId = 0;
    private Project curProject;

    public Project getCurProject() {
        return curProject;
    }

    public void setCurProject(Project curProject) {
        this.curProject = curProject;
    }

    Scanner scanner = new Scanner(System.in);

    public Project createProject(String name) {
        Project project = new Project(name, ++projectId);
        projectsList.add(project);
        curProject = project;
        return project;
    }

    public void projectList() {

        if (projectsList.isEmpty()) {
            System.out.println("LIST EMPTY");
        } else {
            for (int i = 0; i < projectsList.size(); i++) {
                System.out.println(projectsList.get(i).getId() + " : " + projectsList.get(i).getName());
            }
        }

    }

    public void clearProjects() {
        projectsList.clear();
    }

    public void removeProject(int projectId) {
        for (int i = 0; i < projectsList.size(); i++) {
            if (projectId == projectsList.get(i).getId()) {
                System.out.println("Entity.Project " + projectsList.get(i).getName() + " is removed");
                projectsList.remove(projectsList.get(i));
            } else
                System.out.println("PROJECT ID " + projectId + " DOES'T EXIST");
        }
    }

    public void editProject(int projectId) {
        for (int i = 0; i < projectsList.size(); i++) {
            if (projectId == projectsList.get(i).getId()) {
                System.out.println("ENTER NEW NAME");
                projectsList.get(i).setName(scanner.nextLine());
                System.out.println("Entity.Project id " + projectsList.get(i).getId() + " renamed, new name: " + projectsList.get(i).getName());
            } else
                System.out.println("PROJECT ID " + projectId + " DOES'T EXIST");
        }
    }

    public void switchProject (int projectId) {
        for (int i = 0; i < projectsList.size(); i++) {
            if (projectId == projectsList.get(i).getId()) {
                curProject = projectsList.get(i);
                System.out.println("CURRENT PROJECT ID " + projectId);
            }
        }
    }
}
